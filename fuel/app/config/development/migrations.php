<?php
return array(
	'version' => 
	array(
		'app' => 
		array(
			'default' => 
			array(
				0 => '001_create_clientes',
				1 => '002_create_pedidos',
				2 => '003_add_type_to_pedidos',
				3 => '004_delete_birth_from_clientes',
				4 => '005_add_birth_to_clientes',
			),
		),
		'module' => 
		array(
		),
		'package' => 
		array(
			'auth' => 
			array(
				0 => '001_auth_create_usertables',
			),
		),
	),
	'folder' => 'migrations/',
	'table' => 'migration',
);
