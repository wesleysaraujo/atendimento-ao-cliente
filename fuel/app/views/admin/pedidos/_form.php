<?php if (Uri::segment(3) == 'create'): ?>
	<?php echo Form::open(array("action" => "admin/pedidos/".Uri::segment(3)."?cliente=".Input::get('cliente')."&type=".Input::get('type'),"class"=>"form-horizontal")); ?>
<?php else: ?>
	<?php echo Form::open(array("action" => "admin/pedidos/".Uri::segment(3)."/".Uri::segment(4)."?cliente=".Input::get('cliente')."&type=".Input::get('type'),"class"=>"form-horizontal")); ?>
<?php endif ?>
	<fieldset>
		<div class="form-group">
			<div class="col-md-8">
				<?php echo Form::label('Descrição', 'description', array('class'=>'control-label')); ?>
				<?php echo Form::textarea('description', Input::post('description', isset($pedido) ? $pedido->description : ''), array('class' => 'col-md-8 form-control', 'rows' => 10, 'placeholder'=>'Descreva aqui o pedido do cliente')); ?>

			</div>
		</div>
		<?php if (Input::get('type') == 0): ?>
		
			<div class="form-group">
				<div class="col-md-6">
					<?php echo Form::label('Área', 'area', array('class'=>'control-label')); ?>
					<?php echo Form::input('area', Input::post('area', isset($pedido) ? $pedido->area : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'Informe a área que írá resolver o problema')); ?>
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-4">
					<?php echo Form::label('Nº Protocolo', 'protocol', array('class'=>'control-label')); ?>
					<?php echo Form::input('protocol', Input::post('protocol', isset($pedido) ? $pedido->protocol : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'Nº de protocolo desse pedido')); ?>
				</div>
			</div>

		<?php endif ?>
		<div class="form-group">
			<div class="col-md-4">
				<?php echo Form::label('Status', 'status', array('class'=>'control-label')); ?>
				<?php echo Form::select('status', Input::post('status', isset($pedido) ? $pedido->status : ''), array('pendente' => 'Pendente', 'resolvido' => 'Resolvido'), array('class' => 'form-control')); ?>
			</div>
		</div>
		<div class="form-group">
			<div class="col-md-6">
				<div class="btn-group">
					<?php echo Form::submit('submit', 'Salvar', array('class' => 'btn btn-primary')); ?>
					<?php if (Uri::segment(3) == 'edit'): ?>
						<?php echo Html::anchor('admin/pedidos/view/'.$pedido->id, 'Visualizar', array('class' => 'btn btn-warning')); ?>
					<?php endif ?>
					<?php echo Html::anchor('admin/clientes/view/'.Input::get('cliente'), 'Cancelar', array('class' => 'btn btn-danger')); ?>					
				</div>				
			</div>
		</div>
	</fieldset>
<?php echo Form::close(); ?>