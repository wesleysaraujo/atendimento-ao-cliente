<div class="row">
	<div class="col-md-8">
		<p>
			<strong>Tipo do Pedido:</strong>
			<?php echo $tipo = ($pedido->type == 0) ? '<span class="label label-danger">Requerimento</span>' : '<span class="label label-warning">Solicitação</span>'; ?></p>
		<p>
			<strong>Cliente:</strong>
			<?php echo $pedido->cliente->name." ".$pedido->cliente->last_name; ?>
		</p>
		<p>
			<strong>Descrição:</strong>
			<?php echo $pedido->description; ?>
		</p>
		<?php if ($pedido->type == 0): ?>
			<p>
				<strong>Área:</strong>
				<?php echo $pedido->area; ?>
			</p>
			<p>
				<strong>Protocolo:</strong>
				<?php echo $pedido->protocol; ?></p>
			<p>
		<?php endif ?>
		<p>
			<strong>Status:</strong>
			<?php echo $status = ($pedido->status == 'pendente') ? '<span class="label label-danger">'.$pedido->status.'</span>' : '<span class="label label-success">'.$pedido->status.'</span>'; ?>

		</p>	
	</div>
	<div class="col-md-4">
		<ul class="list-group">
			<li class="list-group-item">Cadastrado dia: <?php echo date('d/m/Y H:i:s', $pedido->created_at); ?></li>
			<li class="list-group-item">Alterado dia: <?php echo date('d/m/Y H:i:s', $pedido->updated_at); ?></li>
			<li class="list-group-item"><?php echo Html::anchor('admin/clientes/view/'.$pedido->cliente->id, 'Informações do cliente') ?></li>
		</ul>
	</div>
</div>	
<div class="btn-group">
	<?php echo Html::anchor('admin/pedidos/edit/'.$pedido->id."?cliente=".$pedido->cliente_id."?type=".$pedido->type, '<i class="glyphicon glyphicon-pencil"></i> Editar', array('class' => 'btn btn-warning')); ?>
	<?php echo Html::anchor('admin/clientes/view/'.$pedido->cliente_id, '<i class="glyphicon glyphicon-retweet"></i> Voltar', array('class' => 'btn btn-danger')); ?>
</div>