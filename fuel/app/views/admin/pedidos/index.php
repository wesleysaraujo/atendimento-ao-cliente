<div class="pull-right">
<?php if (Input::get('cliente') and Input::get('cliente') != null): ?>
	<div class="btn-group">
		<?php echo Html::anchor('admin/pedidos/?cliente='.Input::get('cliente').'&filter=resolvido', 'Pedidos Resolvidos', array('class' => 'btn btn-sm btn-success')) ?>		
		<?php echo Html::anchor('admin/pedidos/?cliente='.Input::get('cliente').'&filter=pendente', 'Pedidos Pendentes', array('class' => 'btn btn-sm btn-danger')) ?>		
		<?php echo Html::anchor('admin/clientes/view/'.Input::get('cliente'), 'Voltar', array('class' => 'btn btn-sm btn-warning')) ?>		
	</div>
	<hr>
<?php else: ?>
	<div class="btn-group">
		<?php echo Html::anchor('admin/pedidos/?filter=resolvido', 'Pedidos Resolvidos', array('class' => 'btn btn-sm btn-success')) ?>		
		<?php echo Html::anchor('admin/pedidos/?filter=pendente', 'Pedidos Pendentes', array('class' => 'btn btn-sm btn-danger')) ?>		
		<?php echo Html::anchor('admin/clientes/', 'Voltar', array('class' => 'btn btn-sm btn-warning')) ?>
	</div>
<?php endif; ?>	
</div>
<?php if (!Input::get('cliente')): ?>
	<div>
		<?php echo Form::open(array('class' => 'form-inline')) ?>
			<div class="form-group">
				<h4>Busca por período</h4>
			</div>
			<div class="form-group">
				<?php //echo Form::label('De: ', 'periodo_de', array('class' => 'control-label')) ?>
				<?php echo Form::input('periodo_de', '', array('class' => 'form-control', 'placeholder' => 'Data de início')) ?>
			</div>
			<div class="form-group">
				<?php //echo Form::label('a: ', 'periodo_a', array('class' => 'control-label')) ?>
				<?php echo Form::input('periodo_a', '', array('class' => 'form-control', 'placeholder' => 'Data final')) ?>
			</div>
			<button class="btn btn-primary btn-sm" type="submit">Buscar</button>
		<?php echo Form::close() ?>
	</div>
<hr>
<?php endif; ?>

<?php if ($pedidos): ?>
<table class="table table-striped">
	<thead>
		<tr>
			<th>Tipo</th>
			<th>Cliente</th>
			<th>Descrição</th>
			<th>Área</th>
			<th>Protocolo</th>
			<th>Status</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($pedidos as $item): ?>		<tr>
			<td><?php echo $tipo = ($item->type == 0) ? '<span class="label label-danger">Requerimento</span>' : '<span class="label label-warning">Solicitação</span>' ?></td>
			<td><?php echo $item->cliente->name." ".$item->cliente->last_name; ?></td>
			<td><?php echo Str::truncate($item->description, 60); ?></td>
			<td><?php echo $item->area; ?></td>
			<td><?php echo $item->protocol; ?></td>
			<td><?php echo $status = ($item->status == 'pendente') ? '<span class="label label-danger">'.Inflector::humanize($item->status)."</span>" : '<span class="label label-success">'.Inflector::humanize($item->status)."</span>"; ?></td>
			<td>
				<div class="btn-group">
					<?php echo Html::anchor('admin/pedidos/view/'.$item->id, '<i class="glyphicon glyphicon-eye-open"></i> Ver', array('class'  => 'btn btn-sm btn-info')); ?>
					<?php echo Html::anchor('admin/pedidos/edit/'.$item->id.'?cliente='.$item->cliente_id.'&type='.$item->type, '<i class="glyphicon glyphicon-pencil"></i> Editar', array('class' => 'btn btn-sm btn-warning')); ?>
					<?php echo Html::anchor('admin/pedidos/delete/'.$item->id, '<i class="glyphicon glyphicon-trash"></i> Excluir', array('class' => 'btn btn-sm btn-danger','onclick' => "return confirm('Tem certeza que deseja excluir?')")); ?>
				</div>
			</td>
		</tr>
<?php endforeach; ?>	</tbody>
</table>

<?php else: ?>
<div class="alert alert-warning">
	<p>Nenhum Pedido.</p>
</div>
<?php endif; ?>