<div class="jumbotron">
	<h1>Bem vindo, <?php echo $current_user->username ?>!</h1>
	<p>Seja bem vindo ao sistema de gerenciamento de reclamções de clientes</p>
</div>
<div class="row">
	<div class="col-md-6">
		<h2>Clientes</h2>
		<ul class="list-group">
			<li class="list-group-item">Clientes Cadastrados: <span class="label label-primary"><?php echo count($clientes) ?></span> </li>
			<li class="list-group-item">Usuários do sistema: <span class="label label-success"><?php echo count($users) ?></span></li>
			<li class="list-group-item"></li>
		</ul>
	</div>
	<div class="col-md-6">
		<h2>Pedidos</h2>
		<ul class="list-group">
			<li class="list-group-item">Total de pedidos: <span class="label label-info"><?php echo count($pedidos) ?></span></li>
			<li class="list-group-item">Pedidos Pendentes: <span class="label label-danger"><?php echo count($pedidos_pendentes) ?></span></li>
			<li class="list-group-item">Pedidos Resolvidos: <span class="label label-success"><?php echo count($pedidos_resolvidos) ?></span></li>
		</ul>
	</div>
</div>