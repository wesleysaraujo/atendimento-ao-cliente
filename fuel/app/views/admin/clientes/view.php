<div class="row">
	<div class="pull-right">
		<?php echo Html::anchor('#', '<i class="glyphicon glyphicon-plus-sign"></i> Registrar um Pedido', array('class' => 'btn btn-success', 'data-toggle' => "modal",  'data-target' => "#modal-pedido")); ?>
		<br><br>
	</div>
</div>
<div class="row">
	<div class="col-md-8">
		<p>
			<strong>Nome:</strong>
			<?php echo $cliente->name; ?></p>
		<p>
			<strong>Sobrenome:</strong>
			<?php echo $cliente->last_name; ?></p>
		<p>
			<strong>Data de nascimento:</strong>
			<?php echo $cliente->birth; ?></p>
		<p>
			<strong>RG:</strong>
			<?php echo $cliente->rg; ?></p>
		<p>
			<strong>CPF:</strong>
			<?php echo $cliente->cpf; ?></p>
		<p>
			<strong>Endereço:</strong>
			<?php echo $cliente->address; ?></p>
		<p>
			<strong>Bairro:</strong>
			<?php echo $cliente->district; ?></p>
		<p>
			<strong>CEP:</strong>
			<?php echo $cliente->cep; ?></p>
		<p>
			<strong>Telefone:</strong>
			<?php echo $cliente->phone; ?></p>
		<p>
			<strong>Celular:</strong>
			<?php echo $cliente->celphone; ?></p>
		<p>
			<strong>Cartão do SUS:</strong>
			<?php echo $cliente->card_sus; ?></p>
		<p>
			<strong>E-mail:</strong>
			<?php echo $cliente->email; ?>
		</p>
	</div>
	<div class="col-md-4">
		<ul class="list-group">
			<li class="list-group-item text-primary">Cadastrado em: <?php echo date('d/m/Y H:i:s', $cliente->created_at) ?> </li>
			<li class="list-group-item text-warning">Cadastrato atualizado em: <?php echo date('d/m/Y H:i:s', $cliente->updated_at) ?></li>
			<li class="list-group-item">Total de pedidos: <span class="label label-info"><?php echo count($cliente->pedidos) ?></span> </li>
			<li class="list-group-item">Pedidos pendentes: <span class="label label-danger"><?php echo count($pedidos_pendentes) ?></span> </li>
			<li class="list-group-item">Pedidos resolvidos: <span class="label label-success"><?php echo count($pedidos_resolvidos) ?></span></li>
			<li class="list-group-item"><?php echo Html::anchor('admin/pedidos/?cliente='.$cliente->id, 'Pedidos desse cliente') ?></li>
		</ul>
	</div>
</div>

<div class="row">
	<div class="btn-group">
		<?php echo Html::anchor('admin/clientes/edit/'.$cliente->id, '<i class="glyphicon glyphicon-pencil"></i> Editar informações', array('class' => 'btn btn-warning')); ?>
		<?php echo Html::anchor('admin/clientes', '<i class="glyphicon glyphicon-retweet"></i> Voltar', array('class' => 'btn btn-danger')); ?>
	</div>
</div>

<div class="modal fade" id="modal-pedido">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Registrar novo Pedido</h4>
      </div>
      <div class="modal-body">
        <p>Qual tipo de pedido você deseja registrar?</p>
        <div class="btn-group">
      		<?php echo Html::anchor('admin/pedidos/create?cliente='.$cliente->id.'&type=0', 'Requerimento', array('class' => 'btn btn-lg btn-info')) ?>
      		<?php echo Html::anchor('admin/pedidos/create?cliente='.$cliente->id.'&type=1', 'Solicitação', array('class' => 'btn btn-lg btn-primary')) ?>
  	
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->