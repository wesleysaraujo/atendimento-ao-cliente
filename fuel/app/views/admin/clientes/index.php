<div class="pull-right">
	<div class="row">
		<div class="col-md-5">
			<?php echo Form::open() ?>
				<div class="form-group">
					<div class="input-group">				
						<?php echo Form::input('search', '', array('class' => 'form-control', 'placeholder' => 'Procure por Nome ou CPF')) ?>
						<span class="input-group-btn">
					    	<button class="btn btn-primary" type="submit">Buscar cliente!</button>
					    </span>
					</div>
				</div>
			<?php echo Form::close() ?>
		</div>
		<div class="col-md-5">
			<?php echo Html::anchor('admin/clientes/create', '<i class="glyphicon glyphicon-plus"></i> Cadastrar Cliente', array('class' => 'btn btn-success')); ?>
			<?php echo Html::anchor('admin/clientes/birthdays', '<i class="glyphicon glyphicon-certificate"></i> Aniversariantes do Mês', array('class' => 'btn btn-danger')) ?>
		</div>
	</div>
</div>
<br>
<br>
<?php if ($clientes): ?>
<table class="table table-striped">
	<thead>
		<tr>
			<th>Nome</th>
			<th>Sobrenome</th>
			<th>Telefone</th>
			<th>Celular</th>
			<th>CPF</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($clientes as $item): ?>		<tr>

			<td><?php echo $item->name; ?></td>
			<td><?php echo $item->last_name; ?></td>
			<td><?php echo $item->phone; ?></td>
			<td><?php echo $item->celphone; ?></td>
			<td><?php echo $item->cpf; ?></td>
			<td>
				<div class="btn-group">
					<?php echo Html::anchor('admin/clientes/view/'.$item->id, '<i class="glyphicon glyphicon-eye-open"></i> Visualizar', array('class' => 'btn btn-sm btn-info')); ?>
					<?php echo Html::anchor('admin/clientes/edit/'.$item->id, '<i class="glyphicon glyphicon-pencil"></i> Editar', array('class' => 'btn btn-sm btn-warning')); ?>
					<?php echo Html::anchor('admin/pedidos/?cliente='.$item->id, '<i class="glyphicon glyphicon-list"></i> Pedidos', array('class' => 'btn btn-sm btn-primary')) ?>
					<?php echo Html::anchor('admin/clientes/delete/'.$item->id, '<i class="glyphicon glyphicon-trash"></i> Excluir', array('class' => 'btn btn-sm btn-danger','onclick' => "return confirm('Tem certeza que deseja excluir?')")); ?>
				</div>
			</td>
		</tr>
<?php endforeach; ?>	</tbody>
</table>

<?php else: ?>
	<div class="alert alert-warning">
		<p>Nenhum cliente cadastrado.</p>
	</div>

<?php endif; ?>
