<?php echo Form::open(array("class"=>"form-horizontal")); ?>

	<fieldset>
		<h3>Informações do Cliente</h3>
		<div class="form-group">
			<div class="col-md-4">
				<?php echo Form::label('Nome', 'name', array('class'=>'control-label')); ?>
				<?php echo Form::input('name', Input::post('name', isset($cliente) ? $cliente->name : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'Nome')); ?>	
			</div>
			<div class="col-md-4">
				<?php echo Form::label('Sobrenome', 'last_name', array('class'=>'control-label')); ?>
				<?php echo Form::input('last_name', Input::post('last_name', isset($cliente) ? $cliente->last_name : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'Sobrenome')); ?>
			</div>
			<div class="col-md-4">	
				<?php echo Form::label('Data de nascimento', 'birth', array('class'=>'control-label')); ?>
				<?php echo Form::input('birth', Input::post('birth', isset($cliente) ? $cliente->birth : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'Data de nascimento')); ?>
			</div>
		</div>

		<div class="form-group">
			<div class="col-md-4">
				<?php echo Form::label('RG', 'rg', array('class'=>'control-label')); ?>
				<?php echo Form::input('rg', Input::post('rg', isset($cliente) ? $cliente->rg : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'RG')); ?>
			</div>
			<div class="col-md-4">
				<?php echo Form::label('CPF', 'cpf', array('class'=>'control-label')); ?>
				<?php echo Form::input('cpf', Input::post('cpf', isset($cliente) ? $cliente->cpf : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'CPF')); ?>
			</div>			
		</div>
		<div class="form-group">
			<div class="col-md-5">
				<?php echo Form::label('Endereço', 'address', array('class'=>'control-label')); ?>
				<?php echo Form::input('address', Input::post('address', isset($cliente) ? $cliente->address : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'Endereço')); ?>
			</div>

			<div class="col-md-4">
				<?php echo Form::label('Bairro', 'district', array('class'=>'control-label')); ?>
				<?php echo Form::input('district', Input::post('district', isset($cliente) ? $cliente->district : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'Bairro')); ?>
			</div>
			<div class="col-md-2">
				<?php echo Form::label('CEP', 'cep', array('class'=>'control-label')); ?>
				<?php echo Form::input('cep', Input::post('cep', isset($cliente) ? $cliente->cep : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'CEP')); ?>		
			</div>
		</div>	
		<h3>Informações de contato</h3>
		<div class="form-group">
				<div class="col-md-3">
					<?php echo Form::label('Telefone', 'phone', array('class'=>'control-label')); ?>
					<?php echo Form::input('phone', Input::post('phone', isset($cliente) ? $cliente->phone : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'Telefone')); ?>
				</div>
				<div class="col-md-3">
					<?php echo Form::label('Celular', 'celphone', array('class'=>'control-label')); ?>
					<?php echo Form::input('celphone', Input::post('celphone', isset($cliente) ? $cliente->celphone : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'Celular')); ?>
				</div>
				<div class="col-md-4">
					<?php echo Form::label('E-mail', 'email', array('class'=>'control-label')); ?>
					<?php echo Form::input('email', Input::post('email', isset($cliente) ? $cliente->email : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'E-mail')); ?>
				</div>


				<div class="col-md-4">
					<?php echo Form::label('Cartão do SUS', 'card_sus', array('class'=>'control-label')); ?>
					<?php echo Form::input('card_sus', Input::post('card_sus', isset($cliente) ? $cliente->card_sus : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'Cartão do SUS')); ?>
				</div>
		</div>

		<div class="form-group">
			<label class='control-label'>&nbsp;</label>
			<div class="btn-group">
				<?php echo Form::submit('submit', 'Salvar', array('class' => 'btn btn-primary')); ?>		
				<?php echo Html::anchor('admin/clientes', 'Voltar', array('class' => 'btn btn-danger')); ?>
			</div>
			</div>
	</fieldset>
<?php echo Form::close(); ?>