<br>
<?php if ($clientes): ?>
<table class="table table-striped">
	<thead>
		<tr>
			<th>Nome</th>
			<th>Sobrenome</th>
			<th>Telefone</th>
			<th>Celular</th>
			<th>Data Nascimento</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
<?php 
	foreach ($clientes as $item): 
		$aniversario = explode('-', $item->birth);
		$aniversariante = $aniversario[1];
?>
	<?php if ($aniversariante == date('m')): ?>
	<tr>
	
			<td><?php echo $item->name; ?></td>
			<td><?php echo $item->last_name; ?></td>
			<td><?php echo $item->phone; ?></td>
			<td><?php echo $item->celphone; ?></td>
			<td><?php echo $item->birth; ?></td>
			<td>
				<div class="btn-group">
					<?php echo Html::anchor('admin/clientes/view/'.$item->id, '<i class="glyphicon glyphicon-eye-open"></i> Visualizar', array('class' => 'btn btn-sm btn-info')); ?>
					<?php echo Html::anchor('admin/clientes/edit/'.$item->id, '<i class="glyphicon glyphicon-pencil"></i> Editar', array('class' => 'btn btn-sm btn-warning')); ?>
					<?php echo Html::anchor('admin/pedidos/?cliente='.$item->id, '<i class="glyphicon glyphicon-list"></i> Pedidos', array('class' => 'btn btn-sm btn-primary')) ?>
					<?php echo Html::anchor('admin/clientes/delete/'.$item->id, '<i class="glyphicon glyphicon-trash"></i> Excluir', array('class' => 'btn btn-sm btn-danger','onclick' => "return confirm('Tem certeza que deseja excluir?')")); ?>
				</div>
			</td>
		</tr>
	<?php endif ?>	
<?php endforeach; ?>	</tbody>
</table>

<?php else: ?>
	<div class="alert alert-warning">
		<p>Nenhum cliente cadastrado.</p>
	</div>

<?php endif; ?>
