<div class="row">
	<div class="pull-right">
		<?php echo Html::anchor('admin/users/create', '<i class="glyphicon glyphicon-plus"></i> Cadastrar novo usuário', array('class' => 'btn btn-success')) ?>
	</div>
</div>
<hr>

<?php if ($users): ?>
	<table class="table table-striped">
		<thead>
			<th>Username</th>
			<th>E-mail</th>
			<th>Criado em</th>
			<th></th>
		</thead>
		<tbody>
			<?php foreach ($users as $user): ?>
				<tr>
					<td><?php echo $user->username ?> </td>
					<td><?php echo $user->email ?></td>
					<td><?php echo date('d/m/Y H:i:s', $user->created_at) ?></td>
					<td>
						<div class="btn-group">
							<?php //echo Html::anchor('admin/users/edit/'.$user->id, '<i class="glyphicon glyphicon-pencil"></i> Editar', array('class' => 'btn btn-sm btn-warning')) ?>
							<?php echo Html::anchor('admin/users/delete/'.$user->id, '<i class="glyphicon glyphicon-trash"></i> Excluir', array('class' => 'btn btn-sm btn-danger', 'onclick' => "return confirm('Tem certeza que deseja excluir?')")) ?>
						</div>
					</td>
				</tr>
			<?php endforeach ?>
		</tbody>
	</table>
<?php endif ?>