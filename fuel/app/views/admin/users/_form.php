<div class="row">
	<div class="col-md-6">
		<?php echo Form::open() ?>
			<div class="form-group">
				<?php echo Form::label('Username', 'username') ?>
				<?php echo Form::input('username', Input::post('username', isset($user) ? $user->username : ''), array('class' => 'form-control')) ?>
			</div>

			<div class="form-group">
				<?php echo Form::label('E-mail', 'email') ?>
				<?php echo Form::input('email', Input::post('email', isset($user) ? $user->email : ''), array('class' => 'form-control')) ?>
			</div>
			<div class="form-group">
				<?php if (Uri::segment(3) == 'create'): ?>
					<?php echo Form::label('Senha', 'password') ?>
					<?php echo Form::password('password', '', array('class' => 'form-control')) ?>
				<?php endif ?>
			</div>
			<div class="form-group">
				<div class="btn-group">
					<?php echo Form::submit('submit', 'Salvar', array('class' => 'btn btn-primary')) ?>
					<?php echo Html::anchor('admin/users/', 'Voltar', array('class' => 'btn btn-danger')) ?>				
				</div>
			</div>
		<?php echo Form::close() ?>	
	</div>
</div>