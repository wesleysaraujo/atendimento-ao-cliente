<?php

namespace Fuel\Migrations;

class Delete_birth_from_clientes
{
	public function up()
	{
		\DBUtil::drop_fields('clientes', array(
			'birth'

		));
	}

	public function down()
	{
		\DBUtil::add_fields('clientes', array(
			'birth' => array('constraint' => 11, 'type' => 'int'),

		));
	}
}