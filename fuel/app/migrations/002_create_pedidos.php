<?php

namespace Fuel\Migrations;

class Create_pedidos
{
	public function up()
	{
		\DBUtil::create_table('pedidos', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true, 'unsigned' => true),
			'cliente_id' => array('constraint' => 11, 'type' => 'int'),
			'description' => array('type' => 'text'),
			'area' => array('constraint' => 128, 'type' => 'varchar', 'null' => true),
			'protocol' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'status' => array('constraint' => 64, 'type' => 'varchar'),
			'created_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'updated_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),

		), array('id'));
	}

	public function down()
	{
		\DBUtil::drop_table('pedidos');
	}
}