<?php

namespace Fuel\Migrations;

class Add_birth_to_clientes
{
	public function up()
	{
		\DBUtil::add_fields('clientes', array(
			'birth' => array('constraint' => 10,'type' => 'varchar'),

		));
	}

	public function down()
	{
		\DBUtil::drop_fields('clientes', array(
			'birth'

		));
	}
}