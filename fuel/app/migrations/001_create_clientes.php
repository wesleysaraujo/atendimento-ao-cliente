<?php

namespace Fuel\Migrations;

class Create_clientes
{
	public function up()
	{
		\DBUtil::create_table('clientes', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true, 'unsigned' => true),
			'name' => array('constraint' => 255, 'type' => 'varchar'),
			'last_name' => array('constraint' => 128, 'type' => 'varchar'),
			'birth' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'rg' => array('constraint' => 16, 'type' => 'varchar', 'null' => true),
			'cpf' => array('constraint' => 16, 'type' => 'varchar', 'null' => true),
			'address' => array('constraint' => 255, 'type' => 'varchar'),
			'district' => array('constraint' => 255, 'type' => 'varchar'),
			'cep' => array('constraint' => 9, 'type' => 'varchar'),
			'phone' => array('constraint' => 16, 'type' => 'varchar', 'null' => true),
			'celphone' => array('constraint' => 16, 'type' => 'varchar', 'null' => true),
			'card_sus' => array('constraint' => 64, 'type' => 'varchar', 'null' => true),
			'email' => array('constraint' => 255, 'type' => 'varchar'),
			'created_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'updated_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),

		), array('id'));
	}

	public function down()
	{
		\DBUtil::drop_table('clientes');
	}
}