<?php

namespace Fuel\Migrations;

class Add_type_to_pedidos
{
	public function up()
	{
		\DBUtil::add_fields('pedidos', array(
			'type' => array('constraint' => 11, 'type' => 'int'),

		));
	}

	public function down()
	{
		\DBUtil::drop_fields('pedidos', array(
			'type'

		));
	}
}