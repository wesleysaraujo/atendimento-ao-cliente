<?php

return array(
	'required'        => 'O campo :label obrigatório e precisa conter um valor.',
	'min_length'      => 'O campo :label deve conter no mínimo :param:1 caracteres.',
	'max_length'      => 'O campo :label não deve conter mais que :param:1 caracteres.',
	'exact_length'    => 'O campo :label deve conter exatamente :param:1 caracteres.',
	'match_value'     => 'O campo :label deve conter o valor :param:1.',
	'match_pattern'   => 'O campo :label deve estar no padrão :param:1.',
	'match_field'     => 'O campo :label deve conter o mesmo valor do campo :param:1.',
	'valid_email'     => 'O campo :label deve conter um endereço de e-mail válido.',
	'valid_emails'    => 'O campo :label deve conter uma lista de endereços de e-mails válidos.',
	'valid_url'       => 'O campo :label dever conter uma URL válida.',
	'valid_ip'        => 'O campo :label deve conter um enederço IP válido.',
	'numeric_min'     => 'The minimum numeric value of :label must be :param:1',
	'numeric_max'     => 'The maximum numeric value of :label must be :param:1',
	'numeric_between' => 'The field :label must contain a numeric value between :param:1 and :param:2',
	'valid_string'    => 'The valid string rule :rule(:param:1) failed for field :label',
	'required_with'   => 'The field :label must contain a value if :param:1 contains a value.',
	'valid_date'      => 'The field :label must contain a valid formatted date.',
);
