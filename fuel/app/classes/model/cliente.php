<?php
class Model_Cliente extends \Orm\Model
{
	protected static $_has_many = array('pedidos');

	protected static $_properties = array(
		'id',
		'name',
		'last_name',
		'birth',
		'rg',
		'cpf',
		'address',
		'district',
		'cep',
		'phone',
		'celphone',
		'card_sus',
		'email',
		'created_at',
		'updated_at',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_save'),
			'mysql_timestamp' => false,
		),
	);

	public static function validate($factory)
	{
		$val = Validation::forge($factory);
		$val->add_field('name', 'Nome', 'required|max_length[255]');
		$val->add_field('last_name', 'Sobrenome', 'required|max_length[128]');
		$val->add_field('birth', 'Data de aniversário', 'required');
		$val->add_field('rg', 'RG', 'required|max_length[16]');
		$val->add_field('cpf', 'CPF', 'required|max_length[16]');
		$val->add_field('address', 'Endereço', 'required|max_length[255]');
		$val->add_field('district', 'Bairro', 'required|max_length[255]');
		$val->add_field('cep', 'CEP', 'required|max_length[9]');
		$val->add_field('phone', 'Telefone', 'required|max_length[16]');
		$val->add_field('celphone', 'Celular', 'required|max_length[16]');
		$val->add_field('email', 'Email', 'required|valid_email|max_length[255]');

		return $val;
	}

}
