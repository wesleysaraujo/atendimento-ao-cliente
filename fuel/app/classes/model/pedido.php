<?php
class Model_Pedido extends \Orm\Model
{
	protected static $_belongs_to = array('cliente');

	protected static $_properties = array(
		'id',
		'cliente_id',
		'description',
		'area',
		'protocol',
		'status',
		'created_at',
		'updated_at',
		'type'
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_save'),
			'mysql_timestamp' => false,
		),
	);

	public static function validate($factory)
	{
		$val = Validation::forge($factory);
		$val->add_field('description', 'Descrição', 'required');
		$val->add_field('area', 'Área', 'required|max_length[128]');
		$val->add_field('status', 'Status', 'required|max_length[64]');

		return $val;
	}

	public static function validate_solicite($factory)
	{
		$val = Validation::forge($factory);
		$val->add_field('description', 'Descrição', 'required');
		$val->add_field('status', 'Status', 'required|max_length[64]');

		return $val;
	}

}
