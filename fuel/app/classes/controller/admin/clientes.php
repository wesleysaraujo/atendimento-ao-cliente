<?php
class Controller_Admin_Clientes extends Controller_Admin{

	public function action_index()
	{
		if(!Input::post('search') || Input::post('search') == null)
		{
			$data['clientes'] = Model_Cliente::find('all');
		}
		else
		{
			$data['clientes'] = Model_Cliente::find('all', array('where' => array(
					array('name', Input::post('search')),
					'or' => array(
						array('cpf', Input::post('search')),
					),
				)));
		}
		$this->template->title = "Clientes (".count($data['clientes']).")";
		$this->template->content = View::forge('admin/clientes/index', $data);

	}

	public function  action_birthdays()
	{
		$data['clientes'] = Model_Cliente::find('all');

		$this->template->title = "Aniversariantes do Mês";
		$this->template->content = View::forge('admin/clientes/birthday', $data);

	}

	public function action_view($id = null)
	{
		$data['cliente'] = Model_Cliente::find($id);
		$data['pedidos_pendentes'] = Model_Pedido::find('all', array('where' => array(array('cliente_id', $id), array('status', 'pendente'))));
		$data['pedidos_resolvidos'] = Model_Pedido::find('all', array('where' => array(array('cliente_id', $id), array('status', 'resolvido'))));

		$this->template->title = "Dados do cliente";
		$this->template->content = View::forge('admin/clientes/view', $data);

	}

	public function action_create()
	{
		if (Input::method() == 'POST')
		{
			$val = Model_Cliente::validate('create');

			if ($val->run())
			{
				$cliente = Model_Cliente::forge(array(
					'name' => Input::post('name'),
					'last_name' => Input::post('last_name'),
					'birth' => Input::post('birth'),
					'rg' => Input::post('rg'),
					'cpf' => Input::post('cpf'),
					'address' => Input::post('address'),
					'district' => Input::post('district'),
					'cep' => Input::post('cep'),
					'phone' => Input::post('phone'),
					'celphone' => Input::post('celphone'),
					'card_sus' => Input::post('card_sus'),
					'email' => Input::post('email'),
				));

				if ($cliente and $cliente->save())
				{
					Session::set_flash('success', e('Cliente #'.$cliente->id.' cadastrado com sucesso.'));

					Response::redirect('admin/clientes');
				}

				else
				{
					Session::set_flash('error', e('Não foi possível cadastrar o cliente.'));
				}
			}
			else
			{
				Session::set_flash('error', $val->error());
			}
		}

		$this->template->title = "Cadastro de cliente";
		$this->template->content = View::forge('admin/clientes/create');

	}

	public function action_edit($id = null)
	{
		$cliente = Model_Cliente::find($id);
		$val = Model_Cliente::validate('edit');

		if ($val->run())
		{
			$cliente->name = Input::post('name');
			$cliente->last_name = Input::post('last_name');
			$cliente->birth = Input::post('birth');
			$cliente->rg = Input::post('rg');
			$cliente->cpf = Input::post('cpf');
			$cliente->address = Input::post('address');
			$cliente->district = Input::post('district');
			$cliente->cep = Input::post('cep');
			$cliente->phone = Input::post('phone');
			$cliente->celphone = Input::post('celphone');
			$cliente->card_sus = Input::post('card_sus');
			$cliente->email = Input::post('email');

			if ($cliente->save())
			{
				Session::set_flash('success', e('Cliente atualizado #' . $id));

				Response::redirect('admin/clientes');
			}

			else
			{
				Session::set_flash('error', e('Não possível editar cliente #' . $id));
			}
		}

		else
		{
			if (Input::method() == 'POST')
			{
				$cliente->name = $val->validated('name');
				$cliente->last_name = $val->validated('last_name');
				$cliente->birth = $val->validated('birth');
				$cliente->rg = $val->validated('rg');
				$cliente->cpf = $val->validated('cpf');
				$cliente->address = $val->validated('address');
				$cliente->district = $val->validated('district');
				$cliente->cep = $val->validated('cep');
				$cliente->phone = $val->validated('phone');
				$cliente->celphone = $val->validated('celphone');
				$cliente->card_sus = $val->validated('card_sus');
				$cliente->email = $val->validated('email');

				Session::set_flash('error', $val->error());
			}

			$this->template->set_global('cliente', $cliente, false);
		}

		$this->template->title = "Editando informações do cliente ".$cliente->name;
		$this->template->content = View::forge('admin/clientes/edit');

	}

	public function action_delete($id = null)
	{
		if ($cliente = Model_Cliente::find($id))
		{
			$cliente->delete();

			Session::set_flash('success', e('Cliente excluido com sucesso #'.$id));
		}

		else
		{
			Session::set_flash('error', e('Não foi possível excluir o cliente #'.$id));
		}

		Response::redirect('admin/clientes');

	}


}