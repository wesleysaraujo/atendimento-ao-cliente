<?php
class Controller_Admin_Pedidos extends Controller_Admin{
	public $status = array('pendente' => 'Pendente', 'resolvido' => 'Resolvido');

	public function action_index()
	{
		if(Input::method() == 'POST')
		{
			$de = strtotime(Input::post('periodo_de'));
			$a 	= strtotime(Input::post('periodo_a'));
			$data['pedidos'] = Model_Pedido::find('all', array('where'=> array(array('created_at', 'BETWEEN', array("$de", "$a")))));

			$this->template->title = "Pedidos (".count($data['pedidos']).") entre ".Input::post('periodo_de')." e ".Input::post('periodo_a');
		}
		else
		{
			if(Input::get('cliente') and Input::get('cliente') != '')
			{
				$cliente = Model_Cliente::find(Input::get('cliente'));

				if(Input::get('filter') and Input::get('filter') != null)
				{
					$data['pedidos'] = Model_Pedido::find('all', array('related' => array('cliente'), 'where' => array(array('cliente_id', Input::get('cliente')), array('status', Input::get('filter')))));
				}
				else
				{
					$data['pedidos'] = Model_Pedido::find('all', array('related' => array('cliente'), 'where' => array(array('cliente_id', Input::get('cliente')))));
				}	

				$this->template->title = "Pedidos (".count($data['pedidos']).") cliente: ".$cliente->name." ".$cliente->last_name;
			}
			else
			{
				if(Input::get('filter') and Input::get('filter') != null)
				{
					$data['pedidos'] = Model_Pedido::find('all', array('where' => array(array('status', Input::get('filter')))));	
				}
				else
				{
					$data['pedidos'] = Model_Pedido::find('all');		
				}
				$this->template->title = "Pedidos (".count($data['pedidos']).")";
			}
		}

		
		
		$this->template->content = View::forge('admin/pedidos/index', $data);

	}

	public function action_view($id = null)
	{
		$data['pedido'] = Model_Pedido::find($id);

		$this->template->title = "Pedido";
		$this->template->content = View::forge('admin/pedidos/view', $data);

	}

	public function action_create()
	{
		$cliente = Model_Cliente::find(Input::get('cliente'));

		if(count($cliente) == 0)
		{
			Session::set_flash('error', e('O cliente associado ao pedido não existe'));
			Response::redirect('admin/clientes');
		}

		if(Input::get('type') > 1)
		{
			Session::set_flash('error', e('Esse tipo de pedido não existe'));
			Response::redirect('admin/clientes/view/'.Input::get('cliente'));
		}


		if (Input::method() == 'POST')
		{

			if(Input::get('type') == 0)
			{
				$val = Model_Pedido::validate('create');
			}
			else
			{
				$val = Model_Pedido::validate_solicite('create');
			}

			if ($val->run())
			{
				if(Input::get('type') == 0)
				{
					$pedido = Model_Pedido::forge(array(
						'cliente_id' => $cliente->id,
						'description' => Input::post('description'),
						'area' => Input::post('area'),
						'protocol' => Input::post('protocol'),
						'status' => Input::post('status'),
						'type' => Input::get('type')
					));
				}
				else
				{
					$pedido = Model_Pedido::forge(array(
						'cliente_id' => $cliente->id,
						'description' => Input::post('description'),
						'status' => Input::post('status'),
						'type' => Input::get('type')
					));	
				}

				if ($pedido and $pedido->save())
				{
					
					Session::set_flash('success', e('Pedido #'.$pedido->id.'.'));

					Response::redirect('admin/pedidos/view/'.$pedido->id);
				}

				else
				{
					Session::set_flash('error', e('Não foi possível cadastrar o pedido.'));
				}
			}
			else
			{
				Session::set_flash('error', $val->error());
				//Response::redirect('admin/pedidos/create?cliente='.Input::get('cliente').'&type='.Input::get('type'));
			}
		}

		$this->template->title = "Cadastrar Pedido para o cliente: ".$cliente->name." ".$cliente->last_name;
		$this->template->content = View::forge('admin/pedidos/create');

	}

	public function action_edit($id = null)
	{
		$pedido = Model_Pedido::find($id);
		if($pedido->type == 0)
		{
			$val = Model_Pedido::validate('edit');
		}
		else
		{
			$val = Model_Pedido::validate_solicite('edit');
		}

		if ($val->run())
		{
			if($pedido->type == 0)
			{
				$pedido->description = Input::post('description');
				$pedido->area = Input::post('area');
				$pedido->protocol = Input::post('protocol');
				$pedido->status = Input::post('status');
			}
			else
			{
				$pedido->description = Input::post('description');
				$pedido->status = Input::post('status');
			}

			if ($pedido->save())
			{
				Session::set_flash('success', e('Pedido atualizado com sucesso #' . $id));

				Response::redirect('admin/pedidos/view/'.$id);
			}

			else
			{
				Session::set_flash('error', e('Não foi possível alterar o pedido #' . $id));
			}
		}

		else
		{
			if (Input::method() == 'POST')
			{
				$pedido->cliente_id = $val->validated('cliente_id');
				$pedido->description = $val->validated('description');
				$pedido->area = $val->validated('area');
				$pedido->protocol = $val->validated('protocol');
				$pedido->status = $val->validated('status');

				Session::set_flash('error', $val->error());
			}

			$this->template->set_global('pedido', $pedido, false);
		}

		$this->template->title = "Editar pedido ".$pedido->id;
		$this->template->content = View::forge('admin/pedidos/edit');

	}

	public function action_delete($id = null)
	{
		if ($pedido = Model_Pedido::find($id))
		{
			$pedido->delete();

			Session::set_flash('success', e('Deleted pedido #'.$id));
		}

		else
		{
			Session::set_flash('error', e('Could not delete pedido #'.$id));
		}

		Response::redirect('admin/pedidos');

	}


}