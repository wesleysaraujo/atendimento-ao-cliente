<?php

class Controller_Admin_Users extends Controller_Admin
{

	public function action_index()
	{
		if ($this->current_user->username != 'admin' and $this->current_user->username != 'administrador')
		{
			Response::redirect('admin/');
		}

		$data["users"] = Model_User::find('all');
		$this->template->title = 'Usuários do sistema';
		$this->template->content = View::forge('admin/users/index', $data);
	}

	public function action_view()
	{
		if ($this->current_user->username != 'admin' and $this->current_user->username != 'administrador')
		{
			Response::redirect('admin/');
		}
		

		$data["subnav"] = array('view'=> 'active' );
		$this->template->title = 'Users &raquo; View';
		$this->template->content = View::forge('users/view', $data);
	}

	public function action_create()
	{
		if ($this->current_user->username != 'admin' and $this->current_user->username != 'administrador')
		{
			Response::redirect('admin/');
		}
		
		if (Input::method() == 'POST') {
			$val = Model_User::validate('create');

			if($val->run())
			{
				if(Auth::create_user(Input::post('username'), Input::post('password'), Input::post('email'), 100))
				{
					Session::set_flash('success', e('Novo usuário cadastrado com sucesso'));
					Response::redirect('admin/users');
				}
			}
			else
			{
				Session::set_flash('error', $val->error());
			}
		}

		$this->template->title = 'Cadastrar novo usuário';
		$this->template->content = View::forge('admin/users/create');
	}

	public function action_delete($id = null)
	{
		if ($this->current_user->username != 'admin' and $this->current_user->username != 'administrador')
		{
			Response::redirect('admin/');
		}
		
		if ($user = Model_User::find($id))
		{
			$user->delete();

			Session::set_flash('success', e('Usuário excluido com sucesso #'.$id));
		}

		else
		{
			Session::set_flash('error', e('Não foi possível excluir o usuario #'.$id));
		}

		Response::redirect('admin/users');

	}

}
