var app = {

	init : function()
	{
		app.masks();
	},

	masks : function()
	{

		$('#form_birth').mask('99-99-9999');
		$('#form_periodo_de').mask('99-99-9999 99:99:99');
		$('#form_periodo_a').mask('99-99-9999 99:99:99');
		$('#form_cpf').mask('999.999.999-99');
		$('#form_cep').mask('99999-999');
		$('#form_card_sus').mask('999 9999 9999 9999?9');
		/* Máscara default */
		$.mask.definitions['~']='[123456789]';	
		jQuery('#form_phone, #form_celphone').mask("(~9) ~999-9999?9");
		/*
		*  Script para verificação dos dígitos de 
		*  telefone usando o Masked Input
		*/
		$(document.body).on('keyup', '#form_phone, #form_celphone', function(event){						
		    var target, phone, element;
		    target = $(this);
		    phone = target.val().replace(/\D/g, '');        
		        
		    if(phone.length >= 10) {            
		        target.unmask();
		        if(phone.length > 10) {            
		            target.mask("(~9)~9999-999?9");            
		        } else {
		            target.mask("(~9)~999-9999?9");
		        }
		    }
		}); 
	},

}

jQuery(document).ready(function($) {	
	app.init();
});